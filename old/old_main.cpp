
#include <iostream>
#include <iomanip>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <cmath>

using std::vector;

class negative_param : public std::exception {};
class zero_solution  : public std::exception {};
class inf_solution   : public std::exception {};

const double eps = 0.00001;
typedef unsigned long int big_size_t;

void fill (vector<vector<double> > &A) throw (negative_param)
{
	long int n, dim;
	std::cin >> n;
	std::cin >> dim;
	if (n <= 0 || dim <= 0) {
		throw negative_param();
	}
	dim++;
	A.resize(n);
	for (long int i = 0; i < n; ++i)
	{
		A[i].resize(dim);
		for (long int j = 0; j < dim; ++j) {
			std::cin >> A[i][j];
		}
	}
}


size_t swap_tables (vector<vector<double> > &A, size_t start_ind)
{
	size_t index = start_ind;
	for (size_t j = start_ind+1; j < A[0].size()-1; ++j)
	{
		if ( eps < fabs(A[start_ind][j]) ) {
			index = j;
			for (size_t i = 0; i < A.size(); ++i) {
				std::swap (A[i][start_ind], A[i][index]);
			}
			break;
		}
	}
	return index;
}


size_t swap_string (vector<vector<double> > &A, size_t start_ind)
{
	size_t index = start_ind;
	for (size_t i = start_ind+1; i < A.size(); ++i)
	{
		if ( eps < fabs(A[i][start_ind]) ) {
			index = i;
			std::swap (A[start_ind], A[index]);
			break;
		}
	}
	return index;
}


inline void replace_matrix (vector<vector<double> > &A, size_t start_ind)
{
	double start_ind_value = A[start_ind][start_ind];
	for (size_t k = start_ind+1; k < A.size(); ++k)
	{
		if (fabs(A[k][start_ind]) < eps) continue;
		double tmp = A[k][start_ind];
		for (size_t j = start_ind; j < A[0].size(); ++j)
		{
			A[k][j] = (A[k][j] * start_ind_value / tmp) - A[start_ind][j];
		}
	}
}

vector<size_t>& decomp (vector< vector<double> > &A)
{
	size_t i = 0;
	vector<size_t>& ipvt = *(new vector<size_t>);
	for (size_t i = 0; i < A[0].size()-1; ++i) {
		ipvt.push_back (i);
	}

	while (i < A.size())
	{
		if (fabs(A[i][i]) > eps)
		{
			replace_matrix (A, i);
			i++;
		}
		else if (swap_string(A, i) == i) {
			size_t swp_ind = swap_tables(A, i);
			std::swap (ipvt[i], ipvt[swp_ind]);
			if (swp_ind == i) {
				if (fabs(A[i].back()) > eps) {
					delete &ipvt;
					throw zero_solution();
				}
				size_t old_size = A.size();
				std::swap (A[i], A[old_size-1]);
				A.resize(old_size-1);
			}
		}

	}
	return ipvt;
}


vector<double>& solve (vector<vector<double> > &A, vector<size_t> &ipvt)
{
	vector<double>& x = *(new vector<double>);
	x.resize (A[0].size()-1);
	for (size_t i = 0; i < x.size(); i++) {
		x[i] = NAN;
	}

	for (long int i = A.size()-1; i >= 0; --i)
	{
		double tmp = 0;
		for (size_t j = i+1; j < x.size(); ++j) {
			if (fabs(A[i][j]) > eps) {
				tmp += x[ipvt[j]] * A[i][j];
			}
		}
		if (!isnan(x[i])) {
			delete &x;
			delete &ipvt;
			throw zero_solution();
		}
		x[ipvt[i]] = (A[i].back() - tmp) / A[i][i];
	}
	return x;
}


vector<double>& Gaus (vector<vector<double> > &A) throw (zero_solution, inf_solution)
{
	vector<size_t>& ipvt = decomp (A);

	vector<double>& x = solve (A, ipvt);
	for (size_t i = 0; i < x.size(); i++) {
		if (isnan(x[i])) {
			delete &ipvt;
			delete &x;
			throw inf_solution();
		}
	}

	return x;
}



int main()
{
	vector< vector<double> > A;
	fill(A);
	try {
		vector<double>& x = Gaus (A);
		std::cout << "YES" << std::endl;
		for (size_t i = 0; i < x.size(); i++) {
			std::cout << x[i] << ' ';
		}
		std::cout << '\n';
	}
	catch (zero_solution) {
		std::cout << "NO\n";
	}
	catch (inf_solution) {
		std::cout << "INF\n";
	}

	return 0;
}
